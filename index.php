<?php
    include 'Mahasiswa.php';

    if (!empty($_POST)) {
        if ($_POST['password'] == $_POST['repassword']) {
            //echo $_POST['btn'];
            $mhs = new Mahasiswa($_POST['nim'], $_POST['nama'], $_POST['jenis_kelamin'], "blank.png", $_POST['username'], $_POST['password']);
            
            if (!empty($_FILES['foto']['name'])) {
                $target_dir = "uploads/";
                $target_file = $target_dir . basename($_FILES["foto"]["name"]);
                //$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);
                
                $mhs->foto = basename($_FILES["foto"]["name"]);
            }

            if($_POST['btn']=='add') {
                $mhs->insert();
            }
            elseif($_POST['btn']=='upda') {
                    $mhs->update();
                    header('Location:index.php');
            }
        } else {
            $err_message = 'Confirm Password tidak sama dengan Password';
        }
    }
	
	if(!empty($_GET)){
		$update_status = 1;
		$nim_lama 	   = $_GET['nim'];
		$nama_lama 	   = $_GET['nama'];
		$jk_lama 	   = $_GET['jenis_kelamin'];
        $username_lama = $_GET['username'];
	}
	else
		$update_status = 0;
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Mahasiswa</title>
  </head>
  <body>
    <div class="container">
        <h1>Mahasiswa</h1>

        <?php if (!empty($err_message)) { ?>

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?php echo $err_message; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

        <?php } ?>

        <form method="POST" enctype="multipart/form-data">
            <div class="mb-3">
                <label class="form-label">NIM</label>
                <input type="text" class="form-control" name="nim"
				   <?php
				     if($update_status==1) echo 'value="'.$nim_lama.'" readonly';
				   ?>
				>
            </div>
            <div class="mb-3">
                <label class="form-label">Nama</label>
                <input type="text" class="form-control" name="nama"
				   <?php
				     if($update_status==1) echo 'value="'.$nama_lama.'"';
				   ?>
				>
            </div>
            <div class="mb-3">
                <label class="form-label">Jenis Kelamin</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="Pria" value="Pria" checked>
                    <label class="form-check-label" for="Pria">
                        Pria
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="Wanita" value="Wanita"
						<?php
							if($update_status==1 && $jk_lama=="Wanita") echo 'checked';
						?>
					>
                    <label class="form-check-label" for="Wanita">
                        Wanita
                    </label>
                </div>
            </div>
			<div class="mb-3">
                <label class="form-label">Foto</label>
                <input type="file"  class="form-control" name="foto">
            </div>
            <div class="mb-3">
                <label class="form-label">Username</label>
                <input type="text" class="form-control" name="username"
				   <?php
				     if($update_status==1) echo 'value="'.$username_lama.'"';
				   ?>
				>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="mb-3">
                <label class="form-label">Confirm Password</label>
                <input type="password" class="form-control" name="repassword">
            </div>
			<?php
			  if($update_status==0){
			?>
				<button type="submit" class="btn btn-primary" name="btn" value="add">Add New Data</button>
			<?php
			  }
			  else{
			?>
			    <button type="submit" class="btn btn-success" name="btn" value="upda">Update</button>	
			<?php
			  }
			?>
        </form>

        <br><br>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Jenis Kelamin</th>
					<th scope="col">Username</th>
					<th scope="col">Foto</th>
					<th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>

                <?php
                    $mhs = new Mahasiswa();

                    $i = 1;

                    foreach($mhs->read() as $m) :
                ?>

                <tr>
                    <th scope="row"><?php echo $i++; ?></th>
                    <td><?php echo $m->nim; ?></td>
                    <td><?php echo $m->nama; ?></td>
                    <td><?php echo $m->jenis_kelamin; ?></td>
                    <td><?php echo $m->username; ?></td>
					<td><img src="<?php echo "uploads/".$m->foto; ?>" class="img-thumbnail"></td>
                    <td>
						<a href="Mahasiswa_update.php?nim=<?php echo $m->nim; ?>" class="btn btn-primary btn-sm" >Ubah</a>
                        <a href="Mahasiswa_delete.php?nim=<?php echo $m->nim; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin?');">Hapus</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
    -->
  </body>
</html>