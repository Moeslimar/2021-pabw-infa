<?php
    class DbConn {
        private $host = 'localhost';
        private $username = 'root';
        private $password = '';
        private $db_name = 'evieta';
        private $conn;

        public function __construct() {
            $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->db_name) or die('Connection failed.');
        }

        public function getConn() {
            return $this->conn;
        }
    }
?>